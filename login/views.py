from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout
from .forms import login_form, signin_form

# Create your views here.
def index(request):
	if(request.user.is_authenticated()):
		return(HttpResponse("<center><h1>ALREADY LOGGED IN</h1></center>"))
	
	elif(request.method=='POST'):
		form = login_form(request.POST)
		if(form.is_valid()):
			if(form.login(request)):
				return(HttpResponse("<center><h1>LOGGEDIN</h1></center>"))
			else:
				return(HttpResponse("failed login atempt"))
		else:
			return(HttpResponse("Form errored not valid"))

	else:
		return(render(request,'login/index.html',{"form":login_form}))

def logout_user(request):
	logout(request)
	return(HttpResponseRedirect('/'))

def sign_in(request):
	if(request.method == 'POST'):
		form = signin_form(request.POST)
		if(form.is_valid()):
			if(form.check_username()):
				return(HttpResponse("<center><h1>already eists username</h1></center>"))
			if(form.check_pass()):
				return(HttpResponse("<center<h1>pass mis-match</h1></center>"))
			if(form.add_user):
				return(HttpResponse("<center><h1>User added</h1></center>"))
			else:
				return(HttpResponse("<center><h1>SOME ISSUE!!</h1></center>"))
		else:
			return(HttpResponse(form.errors))
	else:
		return(render(request,'login/signin.html',{'form':signin_form}))
