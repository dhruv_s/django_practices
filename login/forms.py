from django import forms
from django.contrib.auth.models import User
from django.contrib import auth
from .models import *

class login_form(forms.Form):
	username = forms.CharField(label="username",max_length=20,widget=forms.TextInput(attrs={'placeholder':'username'}))
	password = forms.CharField(label="password",max_length=20,widget=forms.PasswordInput(attrs={'placeholder':'Password'}))

	def login(self,request):
		log_user = self.cleaned_data['username']
		log_pass = self.cleaned_data['password']
		user = auth.authenticate(username=log_user, password=log_pass)
		if user and user.is_active:
			auth.login(request,user)
			return True
		else:
			return False


class signin_form(forms.ModelForm):
	username = forms.CharField(label="username",max_length=20,widget=forms.TextInput(attrs={'placeholder':'username'}))
	password = forms.CharField(label="password",max_length=20,widget=forms.PasswordInput(attrs={'placeholder':'Password'}))
	password_rep = forms.CharField(label="password_rep",max_length=20,widget=forms.PasswordInput(attrs={'placeholder':'Repeat Password'}))
	first_name = forms.CharField(label="first_name",max_length=20,widget=forms.TextInput(attrs={'placeholder':'first_name'}))
	last_name = forms.CharField(label="last_name",max_length=20,widget=forms.TextInput(attrs={'placeholder':'last_name'}))
	email = forms.EmailField(label="email",max_length=20,widget=forms.TextInput(attrs={'placeholder':'E-Mail'}))
	colleg = forms.CharField(label="colleg",max_length=20,widget=forms.TextInput(attrs={'placeholder':'colleg name'}))
	graduation_year = forms.CharField(label="graduation_year",max_length=4,widget=forms.TextInput(attrs={'placeholder':'graduation_year'}))
	
	def check_username(self):
		sign_username = self.cleaned_data['user.username']
		if(User.object.get(sign_username__iexact=self.sign_username)):
			return 0
		else:
			return 1

	def check_pass(self):
		sign_pass = self.cleaned_data['password']
		sign_passRe = self.cleaned_data['password_rep']
		if(self.sign_pass==self.sign_passRe):
			return 0
		else:
			return 1

	def add_user(self):
		sign_username = self.cleaned_data['user.username']
		sign_pass = self.cleaned_data['password']
		sign_passRe = self.cleaned_data['password_rep']
		sign_first_name = self.cleaned_data['user.first_name']
		sign_last_name = self.cleaned_data['user.last_name']
		sign_grad_year = self.cleaned_data['graduation_year']
		sign_colleg = self.cleaned_data['colleg']
		sign_email = self.cleaned_data['email']
		user = User.objects.create_user(username= sign_username, password= sign_pass, first_name=sign_first_name, last_name=sign_last_name, email=sign_email)
		entry = details_model(user=user,graduation_year=sign_grad_year,colleg=sign_colleg)
		entry.save()
		user.save()

	class Meta:
		model = details_model
		fields = {"user","graduation_year","colleg"}
