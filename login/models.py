from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class details_model(models.Model):
	user = models.ForeignKey(User)
	graduation_year = models.IntegerField(default=0)
	colleg = models.CharField(max_length=50)
