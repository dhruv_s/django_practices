from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.index,name='index'),
	url(r'^basic/',views.basic,name='basic'),
	url(r'^details/(?P<qu_id>[0-9]+)/',views.details,name = 'details'),
	url(r'^result/(?P<que_id>\d+)/',views.results,name = 'results'),
	url(r'^vote/(?P<que_id>\d+)/',views.vote,name = 'vote'),
]
