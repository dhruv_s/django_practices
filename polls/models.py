import datetime
from django.db import models
from django.utils import timezone

# Create your models here.
class questions(models.Model):
	question_text = models.CharField(max_length=200)
	published = models.DateTimeField('date_published')

	def publish_it(self):
		self.published = timezone.now()
		self.save()
	
	def __str__(self):
		return self.question_text


class choice(models.Model):
	question = models.ForeignKey(questions,on_delete = models.CASCADE)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default = 0)

	def __str__(self):
		return self.choice_text
