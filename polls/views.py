from django.shortcuts import render
from django.http import HttpResponse
from .models import *
# Create your views here.

def index(request):
	return(render(request,'polls/index.html',{}))
	# return(HttpResponse("<h1><center>welcome to polls app</h1></center>"))

def basic(request):
	latest_ques = questions.objects.filter(published__lte = timezone.now()).order_by('published')
	data_fr_page = {"Questions" : latest_ques}
	return(render(request,'polls/basic.html',data_fr_page))

def details(request,qu_id):
	return(render(request,'polls/details.html',{"id":qu_id}))

def results(request):
	return(render(request,'polls/results.html',{}))

def vote(request,q_id):
	return(HttpResponse("<center><h1>U voted for- %s </h1></center>"%(q_id)))
