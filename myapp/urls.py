from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.index,name = 'index'),
	url(r'^hello/',views.hello, name = 'hello'),
	url(r'^article/(?P<day>\d{2})/(?P<year>\d{4})/',views.article,name='article'),
	url(r'^articledoc/(\d+)/(\d+)/',views.article_page,name='article_page'),
	url(r'^add/(\d+)/(\d+)/',views.add, name='add'),
	url(r'^edit/',views.crudops,name= 'crudops'),
	url(r'^modif/',views.manipulate, name = 'manipulate'),
	url(r'^calculator/',views.calculator, name='calculator'),
]
