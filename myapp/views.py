from django.shortcuts import render
from django.http import HttpResponse
from .models import data
# Create your views here.

def index(request):
	return(render(request,'myapp/index.html',{}))

def hello(request):
	return(render(request,'myapp/hello.html',{}))

def article(request, day, year):
	text = """<h1>article id date- %s year= %s</h1>""" % (day,year)
	return(HttpResponse(text))

def article_page(request,art2,art4):
	return(render(request,'myapp/article.html',{"article1" : art2, "article2" : art4}))

def crudops(request):
	#emptying the db
	all_data = data.objects.all()
	all_data.delete()

	#CREATION
	entry = data(name='NO_NAME_MY_NAME1',number = '1123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME2',number = '2123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME3',number = '3123456789')
	entry.save()
	entry = data(name='sum_sum',number = '4123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME4',number = '4123456789')
	entry.save()



	#READ ALL ENTRIES
	objects = data.objects.all()
	res = "<p> printing data of table 'data' i Db : <br> "
	for i in objects:
		res+=(i.name + ' ' + str(i.number) + '<br>')

	res+='<br>'

	#READ SPECIFIC ENTRY
	specific_entry = data.objects.get(name = 'sum_sum')
	res+='the specific entry- <br>'
	res+=specific_entry.name

	res+='<br> <br>'

	#delete entry
	res+='Deleting this 4 entry <br>'
	specific_entry.delete()

	#UPDATE
	entry = data(name='NO_NAME_MY_NAME5',number = '5123456789')
	entry.save()

	res+='<br> <br>'

	#re-print
	all_data = data.objects.all()
	res+='PRINTING AFTER OPERATIONS- <br><br>'
	for i in all_data:
		res+=i.name + '<br>'

	return(HttpResponse(res))

def manipulate(request):
	res=''
	#emptying the db
	all_data = data.objects.all()
	all_data.delete()

	#CREATION
	entry = data(name='NO_NAME_MY_NAME3',number = '3123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME1',number = '1123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME4',number = '4123456789')
	entry.save()
	entry = data(name='sum_sum',number = '4123456789')
	entry.save()
	entry = data(name='NO_NAME_MY_NAME2',number = '2123456789')
	entry.save()

	all_data = data.objects.all()
	res+='found %s number of entries <br>' % (len(all_data))
	for i in all_data	:
		res+=i.name + '<br>'

	res+='<br> re ordering data <br>'
	all_data = data.objects.order_by('name')

	for i in all_data:
		res+=i.name + '<br>'

	res+='<br> through another variable <br>'
	data2 = data.objects.all()
	for i in data2:
		res+=i.name + '<br>'

	return(HttpResponse(res))

def add(request,no1,no2):
	return(render(request,'myapp/add.html',{"no1":no1, "no2":no2, "result":(int(no1)+int(no2))}))

def calculator(request):
	if(request.POST):
		print(request.POST)
		no1 = int(request.POST['no1'][0])
		no2 = int(request.POST['no2'][0])
		return(render(request,'myapp/calculator.html',{"result":(no1+no2)}))
	else:
		return(render(request,'myapp/calculator.html',{}))
